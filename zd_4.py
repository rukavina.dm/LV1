# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 19:41:45 2015

@author: laptop
"""

#pyton 3.4
#rukavina.dm
 # cijena radnog sata
c=0 #pomocna varijabla
g=0 #varijabla trenutne vrijednosti
b=0 #suma vrijednosti
d=0 #inkrementalni brojac unosa
MIN=1000000 #pomocna varijabla za najmanji broj
MAX=0 #pomocna varijabla za najveci broj
while c==0: #petlja za neispravan unos
    try:
    
        a=str(input('Unos broja:')) #unos radnih sati tipa float
               
        if a.startswith('Done'):    # ako je uneseno 'Done'
            c=1                     #program se zaustavlja
            print('Done')
       
        else:                       # u suprotnom program se nastavlja
            g=float(a) #pretvorba u tip float
            d+=1
            b+=g
            if g>MAX:
                MAX=g
            if g<MIN:
                MIN=g
            
            
    except: # ukoliko nije moguce pretvoriti u float pokrece se except
        print('Neispravan unos')
        c=0
print('Broj unesenih brojeva: ',d) #ispis pojedinih vrijednosti
print('Srednja vrijednost: ',b/d)
print('Minimalna vrijednost: ',MIN)
print('Maximalna vrijednost: ',MAX)