# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 19:53:53 2015

@author: laptop
"""
#pyton 3.4
#rukavina.dm
niz=[] #niz za spremanje trenutnih vrijednosti vrijednosti 
fname = input('Enter the file name: ')  #unos imena datoteke
try:
    fhand = open(fname)
except:                             #ukoliko nije moguce otvoriti datoteku
    print ('File cannot be opened:', fname)
    exit()

for line in fhand:   
    if line.startswith('X-DSPAM-Confidence:'): #redak s X-DSPAM-Confidence:
        a=float(line.split(':')[1]) #uzimanje vrijednosti pomocu funkcije split
        niz.append(a)               #dodavanje u niz
print('Srednja vrijednost pouzdanosti: ',sum(niz)/len(niz)) #izracun pomocu ugradenih funkcija